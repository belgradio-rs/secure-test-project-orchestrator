# frozen_string_literal: true

# Module for reporting failed pipelines for the secure test pipelines
module SlackFailureReport
  require_relative 'http_utils'

  PROJECT_ID_CONST = ENV.fetch('CI_PROJECT_ID', 19_722_963).to_i
  FAILURE_MESSAGE = '☠️ QA against Secure test projects failed! ☠️ See '
  BASE_URL =
    "#{ENV.fetch('CI_API_V4_URL')}/projects/#{PROJECT_ID_CONST}/pipelines/"
  CHANNEL = 's_secure-alerts'

  @overall_success = 0
  @overall_failure = 0

  def self.obtain_report_details
    pipeline_output = HttpUtils.paginated_get(
      "#{BASE_URL}#{ENV['CI_PIPELINE_ID']}/bridges"
    )
    child_pipeline_id = pipeline_output.first['downstream_pipeline']['id']
    puts child_pipeline_id
    child_pipeline_bridges_output = HttpUtils.paginated_get(
      "#{BASE_URL}#{child_pipeline_id}/bridges"
    )
    report_results(child_pipeline_bridges_output)
  end

  def self.link_url(child_pipeline_job)
    if !child_pipeline_job['downstream_pipeline']
      child_pipeline_job['pipeline']['web_url']
    else
      child_pipeline_job['downstream_pipeline']['web_url']
    end
  end

  def self.linked_pipeline_name(child_pipeline_job)
    link = link_url(child_pipeline_job)
    @overall_failure += 1
    "<#{link}|❌#{child_pipeline_job['name']}>"
  end

  def self.collect_results(child_pipeline_bridges)
    overall_results = []
    child_pipeline_bridges.each do |child_pipeline_job|
      child_pipeline_hash = {}
      child_pipeline_hash['Status'] = child_pipeline_job['status']

      # As per https://gitlab.com/gitlab-org/quality/ci/secure-test-project-orchestrator/-/issues/4
      # Only display the failures in Slack
      if child_pipeline_job['status'].eql?('success')
        @overall_success += 1
      else
        child_pipeline_hash['Pipeline_name'] =
          linked_pipeline_name(child_pipeline_job)
        overall_results << child_pipeline_hash
      end
    end
    unless overall_results.empty?
      overall_results.sort_by { |k| k['Pipeline_name'] }
      overall_results.sort_by { |k| k['Status'] }
    end
    overall_results
  end

  def self.report_headline_to_slack
    HttpUtils.report_to_slack("#{FAILURE_MESSAGE}#{ENV['CI_PIPELINE_URL']}",
                              'ci_failing',
                              CHANNEL)
    HttpUtils.report_to_slack(
      "#{@overall_failure} Failures, #{@overall_success} Successes",
      'ci_failing',
      CHANNEL
    )
  end

  def self.build_chunked_report_string(chunk_offset_results)
    report_string = ''
    chunk_offset_results.each do |result|
      report_string += "\n#{result['Pipeline_name']}"
    end
    report_string
  end

  def self.report_results(child_pipeline_bridges_output)
    overall_results = collect_results(child_pipeline_bridges_output)
    report_headline_to_slack
    # Chunk the sends to ensure that we don't reach slack message limit
    until overall_results.empty?
      chunk_offset_results = overall_results[0..24]
      HttpUtils.report_to_slack(
        build_chunked_report_string(chunk_offset_results),
        ':ci_failing:',
        CHANNEL
      )
      overall_results = overall_results.drop(25)
    end
  end
end

SlackFailureReport.obtain_report_details
