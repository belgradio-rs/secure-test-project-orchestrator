# frozen_string_literal: true

# Module for shared HTTP utility functions
module HttpUtils
  require 'net/http'
  require 'httparty'
  require 'json'

  STANDARD_HEADERS = {
    'Content-Type' => 'application/json',
    'charset' => 'utf-8'
  }.freeze

  def self.paginated_get(path)
    data = HTTParty.get(path)

    results = JSON.parse(data.body)

    while data.headers['X-Next-Page'] && data.headers['X-Next-Page'] != ''
      data = HTTParty.get("#{path}?page=#{data.headers['X-Next-Page']}")
      results += JSON.parse(data.body)
    end

    results
  end

  def self.report_to_slack(text, icon, slack_channel)
    HTTParty.post(ENV['CI_SLACK_WEBHOOK_URL'],
                  body:
                      JSON.generate({
                                      channel: slack_channel,
                                      username: 'GitLab QA Bot',
                                      text: text,
                                      icon_emoji: icon
                                    }),
                  headers: STANDARD_HEADERS)
  end

  def self.url_exist?(url_string)
    url = URI.parse(url_string)
    req = Net::HTTP.new(url.host, url.port)
    req.use_ssl = (url.scheme == 'https')
    res = req.request_head(url.path || '/')
    res.code != '404' # false if returns 404 - not found
  rescue Errno::ENOENT
    false # false if can't find the server
  end
end
