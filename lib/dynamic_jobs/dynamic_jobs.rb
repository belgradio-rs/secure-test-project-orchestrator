# frozen_string_literal: true

# Module for creating a dyanmic job for the secure test pipelines
module DynamicJobs
  require 'httparty'
  require 'json'
  require_relative 'http_utils'
  require 'logger'

  def self.generate_dynamic_ci
    branches = api_all_branches
    File.open('generated-ci.yml', 'w') do |file|
      iterate_over_project_branches(file, branches)
    end
  end

  def self.api_group_active_projects
    projects = HttpUtils.paginated_get('https://gitlab.com/api/v4/groups/gitlab-org%2Fsecurity-products%2Ftests')
    projects['projects'].reject { |p| p['archived'] }
  end

  def self.api_project(project_id)
    HttpUtils.paginated_get("https://gitlab.com/api/v4/projects/#{project_id}")
  end

  def self.api_project_branches(project_id)
    HttpUtils.paginated_get(
      "https://gitlab.com/api/v4/projects/#{project_id}/repository/branches"
    ).tap do |r|
      r.map { |branch| branch['name'] }
    end
  rescue
    logger.warn("cannot retrieve branches for project #{project_id}, skipping")
    []
  end

  def self.api_all_branches
    projects = api_group_active_projects
    branches = []
    projects.each do |project|
      branches.push(project_branch_details(project))
    end
    branches
  end

  def self.project_branch_details(project)
    branches = {}
    branches['name'] = project['name']
    branches['path_with_namespace'] = project['path_with_namespace']
    branches['branches'] = api_project_branches(project['id']).map { |branch| branch['name'] }
    branches
  end

  def self.output_job(branch, name, path, file)
    file.puts "#{name}-#{branch}:"
    file.puts '  trigger:'
    file.puts '    strategy: depend'
    file.puts "    branch: #{branch}"
    file.puts "    project: #{path}"
    file.puts ''
  end

  def self.iterate_over_project_branches(file, project_branch_array)
    branch_regexp = Regexp.new(ENV.fetch('BRANCH_REGEXP'))
    project_branch_array.each do |project_branch_hash|
      project_regexp_string = if ENV.key?('PROJECT_REGEXP')
                                ENV.fetch('PROJECT_REGEXP')
                              else
                                '.*'
                              end
      project_regexp = Regexp.new(project_regexp_string)
      next unless project_regexp.match(project_branch_hash['name'])

      project_branch_hash['branches'].each do |branch|
        next unless branch_regexp.match(branch)

        output_job(branch,
                   project_branch_hash['name'],
                   project_branch_hash['path_with_namespace'],
                   file)
      end
    end
  end

  def self.logger
    Logger.new(STDOUT)
  end
end
